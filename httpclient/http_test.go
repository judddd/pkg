package httpclient

import (
	"gitee.com/judddd/pkg/trace"
	"net/url"
	"testing"

	"gitee.com/judddd/pkg/log"
)

// TestGet http Get方法 可选择trace log basicAuth
func TestGet(t *testing.T) {
	//初始化trace
	httpTrace := trace.New("http")
	httpTrace.SetLogger(log.ZapLogger())

	// 初始化log
	log.Init(nil)

	// 返回status []byte error
	//_, _, err := Get("http://www.baidu.com", nil, WithLogger(log.ZapLogger()))
	//t.Log(err)
}

// TestGetWithValue url: http://baidu.com?test=values&test2=values
func TestGetWithValue(t *testing.T) {
	params := url.Values{}
	params.Add("test", "values")
	params.Add("test2", "values")
	Get("http://baidu.com", params)
}

func TestGetWithBasic(t *testing.T) {
	Get("www.baidu.com", nil, WithBasicAuth("admin", "123456"))
}
