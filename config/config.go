package config

import (
	"path/filepath"

	"github.com/spf13/viper"
)

func GetEnvInfo(env string) bool {
	//设置的环境变量 想要生效 必须得重启goland
	viper.AutomaticEnv()
	return viper.GetBool(env)
}

func InitConfig(resp interface{}) {
	//	debug := GetEnvInfo("XH_DEBUG")

	v := viper.New()
	//文件的路径如何设置

	realPath := filepath.Join("", "xh-common.yaml")
	v.SetConfigFile(realPath)
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}
	//这个对象如何在其他文件中使用 - 全局变量
	if err := v.Unmarshal(&resp); err != nil {
		panic(err)
	}

}
